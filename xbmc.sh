#!/bin/bash

PI="http://127.0.0.1:8080/jsonrpc"
CURL="curl -i -X POST -H \"Content-Type: application/json\" -d '{\"jsonrpc\": \"2.0\", \"method\": "
CURLEND=", \"params\": { }, \"id\": 1 }' $PI"

while read -r -n1 c
do
	case "$c" in
	h) eval $CURL \"Input.Left\" $CURLEND
		;;
	j) eval $CURL \"Input.Down\" $CURLEND
		;;
	k) eval $CURL \"Input.Up\" $CURLEND
		;;
	l) eval $CURL \"Input.Right\" $CURLEND
		;;
	g) eval $CURL \"Input.Select\" $CURLEND
		;;
	b) eval $CURL \"Input.Back\" $CURLEND
		;;
	c) eval $CURL \"Input.ContextMenu\" $CURLEND
		;;
	q) eval $CURL \"Input.Home\" $CURLEND
		;;
	i) eval $CURL \"Input.Info\" $CURLEND
		;;
	o) eval $CURL \"Input.ShowOSD\" $CURLEND
		;;
	esac	
done
