A simple CLI client for XBMC.

I like the Android XBMC remote controls, but I don't always have my
phone handy. I do, however, have a little ARM netbook ssh'd to my
Raspberry Pi. It's CLI-only, so a webbrowser is out of the
question. Thus, xbmc-cli.

Just start the script and hit one of the shortcut keys. It uses the
JSON API to implement some basic controls:

	* hjkl: left, down, up, right, just like vi
	* g: select
	* b: back
	* c: bring up the context menu
	* q: go to the home menu
	* i: open the info menu
	* o: bring up the OSD menu

With this, you should be able to accomplish pretty much everything you
need. During playback, you can hit "o" to bring up a menu where you
can pause, skip forward, etc.

The script assumes you're sending commands to localhost, because that's how I do things. I also have no idea what happens if you set an http password. I'd be up for any patches to make it more general.